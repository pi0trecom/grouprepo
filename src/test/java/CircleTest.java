import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CircleTest {

    private Shape shape;

    @Test
    public void shouldCalculateArea()
    {
        shape = new Circle(5.25f);

        float area = shape.calculator();

        Assert.assertEquals(86f, area,01f);
    }

    @Test
    public void shouldBigArea(){
        shape = new Circle(80f);

        float area = shape.calculator();

        Assert.assertEquals(20106f, area,0.2f);
    }

}

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class RectangleTest {
    private Shape shape;

    @Test
    public void calculator() throws Exception {
       shape = new Rectangle(2.0f, 3.0f);
        float expected = 6.0f;
        float actual = shape.calculator();
        Assert.assertEquals(expected, actual,0.01);
    }
}
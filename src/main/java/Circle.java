public class Circle implements Shape
{

    private float radius;

    public Circle(float radius) {
        this.radius = radius;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    @Override
    public float calculator()
    {
        float field = (float) (Math.PI * getRadius() * getRadius());
        return field;
    }
}
